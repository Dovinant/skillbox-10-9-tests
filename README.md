# Практическая работа 10.9
Проект содержит каталоги Task01, Task02docker, Task02podman.
Task01 - развёртывает web-сервер в Docker.
Task02docker - развёртывает web-сервер с помощью роли Ansible и выполняет тесты Molecule с драйвером Docker.
Task02podman - развёртывает web-сервер с помощью роли Ansible и выполняет тесты Molecule с драйвером Podman.

## Замечания
### Общие
Пакет testinfra теперь называется pytest-testinfra.

### Task01. Docker, Alpine
Изменён путь к php-alpine.rsa.pub и репозиторию [Подробнее...](https://github.com/codecasts/php-alpine)
В контейнере под управлением Docker не может работать systemd [Подробнее...](https://stackoverflow.com/questions/59466250/docker-system-has-not-been-booted-with-systemd-as-init-system)
Перезапуск служб nginx и php-fpm выполняет supervisord (см. файл Task01/Dockerfile, стр. 65)

Тестируем утилитой pytest - см. файл Task01/Dockerfile, строки 33, 36, 59. Тест проверяет версию OS и Nginx.

### Molecule, Ansible
Ansible и Molecule должны быть установлены через pip, если это не так,то:
> `apt remove ansible` \
> `pip install ansible`

Установим модули проверки синтаксиса
> `pip install "molecule[lint]"`

И включим её добавив в файл molecule/default/molecule.yml строки:
>lint: |\
    set -e\
    yamllint .\
    ansible-lint .\
    flake8 . # Для Task02docker

Модуль pip в Ansible почти всегда вызывает changed даже если нечего не поменялось. Из-за этого не проходит проверка на идемпотентность [Подробнее...](https://github.com/ansible/ansible/issues/28952). Поэтому у меня эта проверка отключена (смотри файл Task02xxx/roles/nginx_php/molecule/default/molecule.yml, строка 34).


### Task02docker
Сначала устанавливаем драйвер для Docker
> `pip install molecule[docker]`

В контейнере под управлением Docker не может работать systemd [Подробнее...](https://stackoverflow.com/questions/59466250/docker-system-has-not-been-booted-with-systemd-as-init-system)
Перезапуск служб nginx и php-fpm выполняет модуль ansible.builtin.sysvinit (см. файлы nginx.yml и php-fpm.yml в каталоге Task02docker/roles/nginx_php/handlers)

Тестируем утилитой testinfra. Т.к. testinfra больше не является утилитой тестированния по умолчанию, поэтому явно указываем её в файле Task02docker/roles/nginx_php/molecule/default/molecule.yml. Сам питоновский файл с тестом лежит по пути Task02docker/roles/nginx_php/molecule/default/tests/test_default.py. Тест проверяет, что пакеты Nginx и PHP установлены, службы запущены и файл test.html содержит определённую строку.

Можно попробовать запустить тесты на удалённой машине определив переменную
> `DOCKER_HOST=«ssh://ansible@your.docker.host»`

### Task02podman
В контейнере под управлением Docker не может работать systemd [Подробнее...](https://stackoverflow.com/questions/59466250/docker-system-has-not-been-booted-with-systemd-as-init-system), однако, эта служба работает в контейнерах Podman. Поэтому для тестов молекулой "железных" серверов Podman подойдёт лучше (IMHO, конечно). Перезапуск служб nginx и php-fpm выполняет модуль ansible.builtin.service (см. файлы nginx.yml и php-fpm.yml в каталоге Task02podman/roles/nginx_php/handlers).

Тестируем самим ansible. Директивы тестирования находятся в файле Task02podman/roles/nginx_php/molecule/default/verify.yml. Тест запрашивает сервер, убеждается что ответ содержит определённую строку, и что тип полученных данных text/html. Также тест выводит на консоль полученный ответ от сервера.

Чтобы Molecule работала с Podman его сначала надо установить. На Ubuntu 20.04 я это делал так:
> `sudo mkdir -p /etc/apt/keyrings`

> `curl -fsSL https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04/Release.key
>  | gpg --dearmor
>  | sudo tee /etc/apt/keyrings/devel_kubic_libcontainers_stable.gpg > /dev/null`

> `echo
>  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/devel_kubic_libcontainers_stable.gpg] 
>    https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04/ /"
>  | sudo tee /etc/apt/sources.list.d/devel_kubic_libcontainers_stable.list > /dev/null`

> `sudo apt-get update -qq`\
> `sudo apt-get -qq -y install podman`

Так же надо установить драйвер для Molecule
> `pip install "molecule[podman]"`

И для работы Podman в rootless
> `sudo apt install containernetworking-plugins` \
> `sudo apt install uidmap`


## Дополнительные материалы

### Molecule
- [Developing and Testing Ansible Roles with Molecule and Podman - Part 1](https://www.ansible.com/blog/developing-and-testing-ansible-roles-with-molecule-and-podman-part-1)
- [Developing and Testing Ansible Roles with Molecule and Podman - Part 2](https://www.ansible.com/blog/developing-and-testing-ansible-roles-with-molecule-and-podman-part-2)
- [Инструкция: как тестировать ansible-роли и узнавать о проблемах до продакшена](https://prohoster.info/blog/administrirovanie/instruktsiya-kak-testirovat-ansible-roli-i-uznavat-o-problemah-do-prodakshena)
- [Эффективная разработка и сопровождение Ansible-ролей](https://habr.com/ru/company/oleg-bunin/blog/431542/)
- [How To Test Ansible Roles with Molecule on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-test-ansible-roles-with-molecule-on-ubuntu-18-04)

### Podman, Docker
- [Основы контейнеризации (обзор Docker и Podman)](https://habr.com/ru/post/659049/)
- [Podman и Buildah для пользователей Docker](https://habr.com/ru/company/redhatrussia/blog/467105/)
- [How does rootless Podman work?](https://opensource.com/article/19/2/how-does-rootless-podman-work)

### Nginx, PHP
- [How To Install Linux, Nginx, MySQL, PHP (LEMP stack) on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-on-ubuntu-20-04)

### Supervisor
- [Настройка supervisor](https://losst.pro/nastrojka-supervisor)
- [ubuntu supervisor manages uwsgi+nginx](https://sourceexample.com/article/en/cd9e8ad9dcfd3243921794926684d592/)
